import React, { useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import VignetteComponent from './components/Vignette';
import SectionJeuxComponent from './components/Jeux/SectionJeux';
import AjouterJeuComposant from './components/Ajouter';
import DropDownPicker from 'react-native-dropdown-picker';

export default function App() {
  const [jeux, setJeux] = useState([
    {
      name: "Medal of Honor",
      price: "10€",
      catégorie: "FPS",
      id: 23124,
      image: "https://m.media-amazon.com/images/I/81Wmhxq3s1S._AC_UF1000,1000_QL80_.jpg"
    },
    {
      name: "Street Fighter 2",
      price: "20€",
      catégorie: "Combat",
      id: 12349,
      image: "https://m.media-amazon.com/images/I/717l7sw-q-L.jpg"
    },
    {
      name: "Call of Duty",
      price: "30€",
      catégorie: "FPS",
      id: 549762,
      image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRu6oCnmOTHiU-a1Cs5vl7I5VKG0M72cxFKTA&s"
    },
    {
      name: "NBA2K5",
      price: "5€",
      catégorie: "Sport",
      id: 549763,
      image: "https://www.picclickimg.com/bxQAAOSwoF1ZwZK~/Sony-PLAYSTATION-2-Espn-NBA-2K5-Jeu-Complet.webp"
    },
    {
      name: "God Of War 2018",
      price: "25€",
      catégorie: "Action-Aventure",
      id: 549764,
      image: "https://image.jeuxvideo.com/medias/151689/1516893501-9622-jaquette-avant.jpg"
    },
    {
      name: "The Legend of Zelda : The Wind Walker",
      price: "35€",
      catégorie: "Action-Aventure",
      id: 549765,
      image: "https://m.media-amazon.com/images/I/81qsls3gdYL._AC_UF1000,1000_QL80_.jpg"
    },
    {
      name: "Horizon : Forbidden West",
      price: "40€",
      catégorie: "Action-Aventure",
      id: 549766,
      image: "https://m.media-amazon.com/images/I/8123PMgTudL._AC_UF1000,1000_QL80_.jpg"
    },
    {
      name: "Forza Horizon 5",
      price: "45€",
      catégorie: "Voiture",
      id: 549767,
      image: "https://m.media-amazon.com/images/I/81qP37GQSEL._AC_UF894,1000_QL80_.jpg"
    },
    {
      name: "The Last Of Us",
      price: "55€",
      catégorie: "Survival horror",
      id: 549768,
      image: "https://m.media-amazon.com/images/I/711XSQPzWRL._AC_UF1000,1000_QL80_.jpg"
    },
    {
      name: "Red Dead Redemption II",
      price: "18€",
      catégorie: "Action-Aventure",
      id: 549769,
      image: "https://media.altchar.com/prod/images/940_530/gm-d4f2d2f7-009f-41d2-8a22-84b8434ac0aa-red-dead-collection.jpg"
    }
  ]);

  const [ouvert, setOuvert] = useState(false);
  const [genreSelectionner, setGenreSelectionner] = useState(null);
  const [genres, setGenres] = useState([
    { label: 'Tous les genres', value: null },
    { label: 'FPS', value: 'FPS' },
    { label: 'Combat', value: 'Combat' },
    { label: 'Sport', value: 'Sport' },
    { label: 'Action-Aventure', value: 'Action-Aventure' },
    { label: 'Voiture', value: 'Voiture' },
  ]);

  const ajouterJeu = (jeu) => {
    setJeux((jeuxActuels) => {
      const nouveauxJeux = [...jeuxActuels, jeu];

      const nouvelleCategorie = jeu.catégorie;
      const genreExiste = genres.some(genre => genre.value === nouvelleCategorie);

      if (!genreExiste) {
        setGenres((genresActuels) => [...genresActuels, { label: nouvelleCategorie, value: nouvelleCategorie }]);
      }

      return nouveauxJeux;
    });
  };


  const supprimerJeu = (id) => {
    setJeux((jeuxActuels) => {
      return jeuxActuels.filter(jeu => jeu.id !== id);
    });
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <VignetteComponent pseudo="Ahmed" nbJeux={jeux.length} />
        <AjouterJeuComposant onAjouter={ajouterJeu} />

        <DropDownPicker
          open={ouvert}
          value={genreSelectionner}
          items={genres}
          setOpen={setOuvert}
          setValue={setGenreSelectionner}
          setItems={setGenres}
          placeholder="Ctaégorie de jeu"
          containerStyle={{ height: 40, width: 200, marginTop: 20 }}
          style={{ backgroundColor: '#fafafa' }}
          dropDownContainerStyle={{ backgroundColor: '#fafafa' }}
        />

        <SectionJeuxComponent jeux={jeux.filter(jeu => !genreSelectionner || jeu.catégorie === genreSelectionner)} onSupprimer={supprimerJeu} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
});

