import React, { useState } from 'react';
import { Button, TextInput, View, StyleSheet, Text } from 'react-native';

const AjouterJeuComposant = ({ onAjouter }) => {
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [catégorie, setCategorie] = useState('');

    const ajouterJeu = () => {
        onAjouter({ name, price, catégorie });
        setName('');
        setPrice('');
        setCategorie('');
    };

    return (
        <View style={styles.container}>
            <Text style={styles.label}>Titre :</Text>
            <TextInput
                style={styles.input}
                placeholder="Entrez le titre du jeu"
                value={name}
                onChangeText={setName}
                placeholderTextColor="#888"
            />
            <Text style={styles.label}>Tarif :</Text>
            <TextInput
                style={styles.input}
                placeholder="Entrez le tarif du jeu"
                value={price}
                onChangeText={setPrice}
                placeholderTextColor="#888"
                keyboardType="numeric"
            />
            <Text style={styles.label}>Catégorie :</Text>
            <TextInput
                style={styles.input}
                placeholder="Entrez la catégorie du jeu"
                value={catégorie}
                onChangeText={setCategorie}
                placeholderTextColor="#888"
            />
            <Button title="Ajouter le jeu" onPress={ajouterJeu} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 20,
        backgroundColor: '#f0f8ff',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4.65,
        elevation: 8,
        alignItems: 'center',
        width: '90%',
        alignSelf: 'center',
        marginTop: 20,
    },
    input: {
        height: 40,
        width: '100%',
        borderColor: '#ccc',
        borderWidth: 1,
        marginBottom: 15,
        paddingHorizontal: 10,
        borderRadius: 5,
    },
    label: {
        fontWeight: 'bold',
        marginBottom: 5,
        fontSize: 16,
        color: '#333',
        alignSelf: 'flex-start',
    },
});

export default AjouterJeuComposant;
