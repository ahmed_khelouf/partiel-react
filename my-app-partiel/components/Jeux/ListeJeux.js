import React from 'react';
import { View, StyleSheet } from 'react-native';
import JeuComponent from './Jeu';

const ListeJeuxComponent = ({ jeux, onSupprimer }) => {
    return (
        <View style={styles.listeContainer}>
            {jeux.map((jeu) => <JeuComponent key={jeu.id} jeu={jeu} onSupprimer={onSupprimer} />)}
        </View>
    );
}

const styles = StyleSheet.create({
    listeContainer: {
        marginTop: 20, 
        width: '100%',
        marginBottom: 80, 
        paddingHorizontal: 10, 
    },
});

export default ListeJeuxComponent;
