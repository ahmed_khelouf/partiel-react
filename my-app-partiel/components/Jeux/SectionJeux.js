import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ListeJeuxComponent from './ListeJeux';

const SectionJeuxComponent = ({ jeux, onSupprimer }) => {
    return (
        <View style={styles.sectionContainer}>
            <Text style={styles.title}>Ma liste de jeux</Text>
            <ListeJeuxComponent jeux={jeux} onSupprimer={onSupprimer} />
        </View>
    );
}

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 30,
        alignItems: 'center',
        borderRadius: 15,
        width: '95%',
        padding: 25,
        backgroundColor: '#1E90FF',
        shadowColor: "#000",
    },
    title: {
        color: '#FFFFFF',
        fontSize: 24,
        marginBottom: 20,
        fontWeight: 'bold',
    },
});

export default SectionJeuxComponent;