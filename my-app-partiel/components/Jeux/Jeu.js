import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

const JeuComponent = ({ jeu, onSupprimer }) => {
    return (
        <View style={styles.jeuContainer}>
            <View style={styles.contentContainer}>
                <Image
                    style={styles.image}
                    source={{ uri: jeu.image }}
                />
                <View style={styles.textContainer}>
                    <Text style={styles.gameName}>Nom du jeu: {jeu.name}</Text>
                    <Text style={styles.text}>Genre: {jeu.catégorie}</Text>
                    <Text style={styles.text}>Prix: {jeu.price}</Text>
                </View>
            </View>
            <TouchableOpacity onPress={() => onSupprimer(jeu.id)} style={styles.buttonContainer}>
                <Text style={styles.buttonText}>Supprimer</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    jeuContainer: {
        backgroundColor: '#1E90FF',
        borderRadius: 10,
        width: '90%',
        padding: 15,
        marginBottom: 20,
        borderWidth: 1,
        borderColor: '#FFFFFF',
        shadowColor: "#000",
    },
    contentContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textContainer: {
        marginLeft: 15,
        flex: 1,
        justifyContent: 'center',
    },
    image: {
        width: 100,
        height: 120,
        borderRadius: 10,
    },
    text: {
        color: '#FFFFFF',
        fontSize: 16,
        marginBottom: 5,
    },
    gameName: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 10,
    },
    buttonContainer: {
        backgroundColor: 'red',
        marginTop: 10,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 5,
        alignSelf: 'center',
    },
    buttonText: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 16,
    },
});

export default JeuComponent;
