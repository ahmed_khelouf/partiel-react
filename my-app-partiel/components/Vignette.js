import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const VignetteComponent = ({ pseudo, nbJeux }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Je suis {pseudo} et j'ai {nbJeux} jeux !</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        alignItems: 'center',
        backgroundColor: '#1E90FF',
        borderRadius: 10,
        width: '90%',
        padding: 20,
        shadowColor: "#000",
        alignSelf: 'center',
    },
    text: {
        color: '#FFFFFF',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    }
});

export default VignetteComponent;
